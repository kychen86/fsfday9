var express = require("express")

var app = express();



app.use(express.static(__dirname +"/public"));
app.use("/lib", express.static(__dirname + "/bower_components"));

//
if (process.env.PROD){
    //Production configuration
    //if PROD is set, then it will run here, else on else
    console.log("Running on prod");
}else{
    //testing configuration
    console.log("Testing");
}

var port = parseInt(process.argv[2])||process.env.APP_PORT || 3000 //Check command input first, then look at env APP_PORT for number otherwise use 3000
app.listen(port,function(){
    console.log("using port %d", port);
});