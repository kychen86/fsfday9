(function(){    
    var TodoApp = angular.module("TodoApp",[]);

    var TodoCtrl = function(){
        todoCtrl = this;
        todoCtrl.todoItem = "";
        todoCtrl.todoList = [];
        todoCtrl.searchText = "";
        todoCtrl.addToList = function(){
            todoCtrl.todoList.push(todoCtrl.todoItem);
            todoCtrl.todoItem = "";
            console.log("todo list\n" + todoCtrl.todoList);
        }

        todoCtrl.delete=function($index){
            todoCtrl.todoList.splice($index,1);
        }

    }


TodoApp.controller("TodoCtrl", TodoCtrl);
})();